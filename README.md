uncap\_ex.vim
=============

This plugin defines custom commands like `:W`, `:Qa`, and `:Wq` to match their
lowercase analogues, to forgive me when my pinky finger doesn't roll off the
Shift key quite soon enough after pressing the colon key.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
